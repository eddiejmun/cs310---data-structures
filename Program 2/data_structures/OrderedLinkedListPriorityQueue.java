/*
Program #2
Edward Mun
cssc0924
*/
package data_structures;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
public class OrderedLinkedListPriorityQueue<E extends Comparable <E>>
implements PriorityQueue<E> {
        private Node<E> head;
        private int currentSize;
        private long modCounter;
        public OrderedLinkedListPriorityQueue() {
                 currentSize = 0;
head = null; }
        private class Node<T> {
                 T data;
                 Node<T> next;
        public Node(T data) {
                 this.data = data;
next = null;
} }
    //  Inserts a new object into the priority queue.  Returns true if
    //  the insertion is successful.  If the PQ is full, the insertion
    //  is aborted, and the method returns false.
    public boolean insert(E obj)  {
        if (isFull()) {
                 return false;
                 }
        Node<E> newNode = new Node(obj);
        Node<E> previous = null, current = head;
        while (current != null &&
                 (newNode.data.compareTo(current.data) >= 0)) {
                         previous = current;
                         current = current.next;
                         }
modCounter++;
currentSize++;
return true;
}
if (previous == null) {
        newNode.next = head;
        head = newNode;
} else {
previous.next = newNode;
newNode.next = current;
}
    //  Removes the object of highest priority that has been in the
    //  PQ the longest, and returns it.  Returns null if the PQ is
empty.
    public E remove() {
        if(isEmpty()) {
return null;
                 }
        E best = head.data;
        head = head.next;
        modCounter++;
        currentSize--;
        return best;
}
    //  Deletes all instances of the parameter obj from the PQ if
found, and
    //  returns true.  Returns false if no match to the parameter obj
is found.
    public boolean delete(E obj) {
        Node<E> previous = null, current = head;
        boolean ynDelete = false;
        while (head != null && (obj.compareTo(head.data) == 0)) {
                 head = head.next;
                 currentSize--;
                 modCounter++;
                 ynDelete = true;
                 }
        if (head == null) {
                 return false;
                 }
        current = head;
        while (current.next != null) {
                 if (obj.compareTo(current.next.data) == 0) {
                         current.next = current.next.next;
                         currentSize--;
                         modCounter++;
                         ynDelete = true;
                 } else {
                         current = current.next;
} }
                 return ynDelete;
}
    //  Returns the object of highest priority that has been in the
    //  PQ the longest, but does NOT remove it.
    //  Returns null if the PQ is empty.
    public E peek() {
        if(isEmpty()) {
                 return null;
        }
        E best = head.data;
        return best;
        }
    //  Returns true if the priority queue contains the specified
element
    //  false otherwise.
    public boolean contains(E obj) {
        Node<E> temp = head;
        while (temp != null) {
                 if (temp.data.compareTo(obj) == 0) {
                         return true;
                         }
                 temp = temp.next;
                 }
        return false;
}
    //  Returns the number of objects currently in the PQ.
    public int size() {
        return currentSize;
        }
    //  Returns the PQ to an empty state.
    public void clear() {
        currentSize = 0;
        modCounter++;
        head = null;
        }
    //  Returns true if the PQ is empty, otherwise false
    public boolean isEmpty() {
        return currentSize == 0;
        }
//  Returns true if the PQ is full, otherwise false.  List based
    //  implementations should always return false.
    public boolean isFull() {
        return false;
        }
    //  Returns an iterator of the objects in the PQ, in no particular
    //  order.
    public Iterator<E> iterator() {
        return new IteratorHelper();
    }
    class IteratorHelper implements Iterator<E> {
        Node<E> iterIndex;
        long stateCheck;
        public IteratorHelper() {
            iterIndex = head;
            stateCheck = modCounter;
}
        public boolean hasNext() {
            if (stateCheck != modCounter)
                throw new ConcurrentModificationException();
            return iterIndex != null;
}
        public E next() {
            if (!hasNext())
                throw new NoSuchElementException();
            E temp = iterIndex.data;
            iterIndex = iterIndex.next;
            return temp;
}
        public void remove() {
            throw new UnsupportedOperationException();
} }
}
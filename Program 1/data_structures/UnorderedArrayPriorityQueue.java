/*
Program #1
Edward Mun
cssc0924
*/
package data_structures;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
public class UnorderedArrayPriorityQueue<E extends Comparable <E>>
implements PriorityQueue<E> {
    private E [] storage;
    private int currentSize;
    private int maxSize;
    private long modCounter;
    public UnorderedArrayPriorityQueue(int size) {
        maxSize = size;
        storage = (E[]) new Comparable[maxSize];
        currentSize = 0;
        modCounter = 0;
        }
    public UnorderedArrayPriorityQueue() {
        this(DEFAULT_MAX_CAPACITY);
        }
    //  Inserts a new object into the priority queue.  Returns true if
    //  the insertion is successful.  If the PQ is full, the insertion
    //  is aborted, and the method returns false.
    public boolean insert(E object) {
        if(isFull()) {
                 return false;
                 }
        currentSize++;
        storage[currentSize-1] = object;
        return true;
        }
    //  Removes the object of highest priority that has been in the
    //  PQ the longest, and returns it.  Returns null if the PQ is
empty.
    public E remove() {
        if(isEmpty()) {
return null;
                 }
        int highest = 0;
        for(int i = 1; i < currentSize; i++) {
if(((Comparable<E>)storage[highest]).compareTo(storage[i]) <= 0) {
                         highest = highest;
}
if(((Comparable<E>)storage[highest]).compareTo(storage[i]) > 0) {
                         highest = i;
} }
        E holder = storage[highest];
        for(int i = highest; i < currentSize-1; i++) {
                 storage[i] = storage[i+1];
                 }
        currentSize--;
        modCounter++;
        return holder;
        }
    //  Deletes all instances of the parameter obj from the PQ if
found, and
    //  returns true.  Returns false if no match to the parameter obj
is found.
    public boolean delete(E obj) {
        int didChange = 0;
        for(int i = 0; i < currentSize; i++) {
} }
if(((Comparable<E>)obj).compareTo(storage[i]) == 0) {
        for(int k = i; k < currentSize-1; k++) {
                 storage[k] = storage[k+1];
                 }
        currentSize--;
        i--;
        modCounter++;
        didChange++;
        }
return didChange != 0;
//  Returns the object of highest priority that has been in the
//  PQ the longest, but does NOT remove it.
//  Returns null if the PQ is empty.
public E peek() {
        if (isEmpty()) {
                 return null;
                 }
        int highest = 0;
        for(int i = 1; i < currentSize; i++) {
if(((Comparable<E>)storage[highest]).compareTo(storage[i]) <= 0) {
                         highest = highest;
}
if(((Comparable<E>)storage[highest]).compareTo(storage[i]) > 0) {
                         highest = i;
                 }
                 }
                 return storage[highest];
}
    //  Returns true if the priority queue contains the specified
element
    //  false otherwise.
    public boolean contains(E obj) {
        for(int i = 0; i < currentSize; i++) {
                 if(((Comparable<E>)obj).compareTo(storage[i]) == 0)
        }
return false;
}
return true;
//  Returns the number of objects currently in the PQ.
public int size() {
    return currentSize;
    }
//  Returns the PQ to an empty state.
public void clear() {
    for(int i = 0; i < currentSize; i++) {
             storage[i] = null;
             }
    modCounter++;
    currentSize = 0;
    }
//  Returns true if the PQ is empty, otherwise false
public boolean isEmpty() {
    return currentSize == 0;
    }
//  Returns true if the PQ is full, otherwise false.  List based
//  implementations should always return false.
public boolean isFull() {
    return currentSize == maxSize;
    }
//  Returns an iterator of the objects in the PQ, in no particular
//  order.
public Iterator<E> iterator() {
    return new IteratorHelper();
    }
class IteratorHelper implements Iterator<E> {
    int iterIndex;
    long stateCheck;
    public IteratorHelper() {
             iterIndex = 0;
             stateCheck = modCounter;
             }
    public boolean hasNext() {
             if(stateCheck != modCounter) {
}
        throw new ConcurrentModificationException();
        }
return iterIndex < currentSize;
public E next() {
        if(!hasNext()) {
}
        throw new NoSuchElementException();
        }
return storage[iterIndex++];
        public void remove() {
                 throw new UnsupportedOperationException();
} }
}
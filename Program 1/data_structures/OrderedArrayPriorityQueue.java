/*
Program #1
Edward Mun
cssc0924
*/
package data_structures;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
public class OrderedArrayPriorityQueue<E extends Comparable <E>>
implements PriorityQueue<E> {
    private E [] storage;
    private int currentSize;
    private int maxSize;
    private long modCounter;
    public OrderedArrayPriorityQueue(int size) {
        maxSize = size;
        storage = (E[]) new Comparable[maxSize];
        currentSize = 0;
        modCounter = 0;
        }
    public OrderedArrayPriorityQueue() {
        this(DEFAULT_MAX_CAPACITY);
        }
    //  Inserts a new object into the priority queue.  Returns true if
    //  the insertion is successful.  If the PQ is full, the insertion
    //  is aborted, and the method returns false.
    public boolean insert(E object) {
        if(isFull()) {
                 return false;
}
        int where = findInsertionPoint(object, 0, currentSize-1);
        for(int i=currentSize-1; i >= where; i--) {
                 storage[i+1] = storage[i];
                 }
        storage[where] = object;
        currentSize++;
        modCounter++;
        return true;
        }
== 0) {
>= 0) {
<= 0) {
int middle = (lo + hi) / 2;
if( ((Comparable<E>)obj).compareTo(storage[middle])
        return middle;
if( ((Comparable<E>)obj).compareTo(storage[middle])
        hi = middle - 1;
if( ((Comparable<E>)obj).compareTo(storage[middle])
        lo = middle + 1;
private int findInsertionPoint(E obj, int lo, int hi) {
    if(hi < lo)
             return lo;
    int mid = (lo+hi) >> 1;
    if( ((Comparable<E>)obj).compareTo(storage[mid]) >= 0)
             return findInsertionPoint(obj, lo, mid-1);
    return findInsertionPoint(obj, mid+1, hi);
    }
private int binarySearch(E obj, int lo, int hi) {
    while(hi >= lo) {
} }
return -1; }
}
}
    //  Removes the object of highest priority that has been in the
    //  PQ the longest, and returns it.  Returns null if the PQ is
empty.
    public E remove() {
        if(isEmpty()) {
return null;
                 }
        modCounter++;
        return storage[--currentSize];
        }
    //  Deletes all instances of the parameter obj from the PQ if
found, and
    //  returns true.  Returns false if no match to the parameter obj
is found.
    public boolean delete(E obj) {
        int where = binarySearch(obj, 0, currentSize -1);
        if(where == -1) {
                 return false;
                 }
        int counter = 0;
        int point = where;
        while((point >= 0) &&
(((Comparable<E>)obj).compareTo(storage[point]) == 0)) {
                 point--;
                 counter++;
                 }
        point = where+1;
        while((point <= currentSize-1) &&
(((Comparable<E>)obj).compareTo(storage[point]) == 0)) {
                 point++;
                 counter++;
                 }
        int start = point - counter;
        for (int i = start; i < currentSize-counter; i++) {
                 storage[start] = storage[point];
                 start++;
                 point++;
                 }
        currentSize = currentSize - counter;
        modCounter++;
        return true;
        }
    //  Returns the object of highest priority that has been in the
    //  PQ the longest, but does NOT remove it.
    //  Returns null if the PQ is empty.
    public E peek() {
        if(currentSize == 0) {
                 return null;
                 }
        return storage[currentSize-1];
}
    //  Returns true if the priority queue contains the specified
element
    //  false otherwise.
    public boolean contains(E obj) {
        if(binarySearch(obj,0,currentSize-1) == -1) {
                 return false;
                 }
        return true;
}
//  Returns the number of objects currently in the PQ.
public int size() {
    return currentSize;
    }
//  Returns the PQ to an empty state.
public void clear() {
    for(int i = 0; i < currentSize; i++) {
             storage[i] = null;
             }
    currentSize = 0;
    modCounter++;
    }
//  Returns true if the PQ is empty, otherwise false
public boolean isEmpty() {
    return currentSize == 0;
    }
//  Returns true if the PQ is full, otherwise false.  List based
//  implementations should always return false.
public boolean isFull() {
    return currentSize == maxSize;
    }
//  Returns an iterator of the objects in the PQ, in no particular
//  order.
public Iterator<E> iterator() {
    return new IteratorHelper();
    }
class IteratorHelper implements Iterator<E> {
    int iterIndex;
    long stateCheck;
    public IteratorHelper() {
             iterIndex = 0;
             stateCheck = modCounter;
             }
    public boolean hasNext() {
             if(stateCheck != modCounter) {
}
        throw new ConcurrentModificationException();
        }
return iterIndex < currentSize;
public E next() {
        if(!hasNext()) {
                 throw new NoSuchElementException();
}
        }
return storage[iterIndex++];
        public void remove() {
                 throw new UnsupportedOperationException();
} }
}
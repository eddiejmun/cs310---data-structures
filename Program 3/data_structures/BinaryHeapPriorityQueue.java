/*
Edward Mun
cssc0924
Prog 3
*/
package data_structures;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
public class BinaryHeapPriorityQueue<E extends Comparable <E>>
implements PriorityQueue<E> {
    private Wrapper<E> [] storage;
    private long modCounter;
    private int currentSize;
    private long entryNumber;
    private int maxSize;
    public BinaryHeapPriorityQueue(int size) {
        entryNumber = 0;
        storage = (Wrapper<E>[]) new Wrapper[size];
        maxSize = size;
        currentSize = 0;
        modCounter = 0;
        }
    public BinaryHeapPriorityQueue() {
        this(DEFAULT_MAX_CAPACITY);
        }
    protected class Wrapper<E> implements Comparable<Wrapper<E>> {
        long number;
E data;
    public Wrapper(E d) {
        number = entryNumber++;
        data = d;
        }
    public int compareTo(Wrapper<E> o) {
        if(((Comparable<E>)data).compareTo(o.data) == 0)
                 return (int) (number - o.number);
        return ((Comparable<E>)data).compareTo(o.data);
        }
}
    private void trickleUp() {
        int newIndex = currentSize-1;
        int parentIndex = (newIndex-1) >> 1;
        Wrapper<E> newValue = storage[newIndex];
        while(parentIndex >= 0 &&
newValue.compareTo(storage[parentIndex]) < 0) {
                 storage[newIndex] = storage[parentIndex];
                 newIndex = parentIndex;
                 parentIndex = (parentIndex-1) >> 1;
                 }
        storage[newIndex] = newValue;
        }
    private void trickleDown() {
        int current = 0;
        int child = getNextChild(current);
        while(child != -1 &&
storage[current].compareTo(storage[child]) < 0 &&
{
             storage[child].compareTo(storage[currentSize-1]) < 0)
         storage[current] = storage[child];
         current = child;
         child = getNextChild(current);
         }
    storage[current] = storage[currentSize-1];
    }
private int getNextChild(int current) {
    int left = (current << 1) + 1;
    int right = left+1;
    if(right < currentSize) {
             if(storage[left].compareTo(storage[right]) < 0)
                     return left;
             return right;
             }
    if(left < currentSize)
             return left;
    return -1;
}
//  Inserts a new object into the priority queue.  Returns true if
//  the insertion is successful.  If the PQ is full, the insertion
//  is aborted, and the method returns false.
public boolean insert(E object) {
    if (isFull())
             return false;
    currentSize++;
        storage[currentSize-1] = new Wrapper<E> (object);
        trickleUp();
        modCounter++;
        return true;
}
    //  Removes the object of highest priority that has been in the
    //  PQ the longest, and returns it.  Returns null if the PQ is
empty.
    public E remove() {
        if(isEmpty())
                 return null;
        E temp = (E) storage[0].data;
        trickleDown();
        currentSize--;
        modCounter++;
        return temp;
}
    //  Deletes all instances of the parameter obj from the PQ if
found, and
    //  returns true.  Returns false if no match to the parameter obj
is found.
    public boolean delete(E obj) {
        boolean yesOrNo = false;
        int index = 0;
        Wrapper<E> [] tempArray = new Wrapper[currentSize];
        for (int i = 0; i < currentSize; i++) {
                 E temp = (E) remove();
                 if(((Comparable<E>)obj).compareTo(temp) != 0) {
                         tempArray[index++] = new Wrapper<E>(temp);
} else {
                         yesOrNo = true;
} }
        for (int k = 0; k < currentSize; k++) {
                 insert(tempArray[k].data);
                 }
        if (yesOrNo = true)
                 modCounter++;
        return yesOrNo;
}
    //  Returns the object of highest priority that has been in the
    //  PQ the longest, but does NOT remove it.
    //  Returns null if the PQ is empty.
    public E peek() {
        if(isEmpty())
                 return null;
        return storage[0].data;
        }
    //  Returns true if the priority queue contains the specified
element
    //  false otherwise.
    public boolean contains(E obj) {
0) {
    for (int i = 0; i < currentSize; i++) {
             if(((Comparable<E>)obj).compareTo(storage[i].data) ==
return true;
} }
    return false;
    }
//  Returns the number of objects currently in the PQ.
public int size() {
    return currentSize;
    }
//  Returns the PQ to an empty state.
public void clear() {
    currentSize = 0;
    modCounter++;
    }
//  Returns true if the PQ is empty, otherwise false
public boolean isEmpty() {
    return currentSize == 0;
    }
//  Returns true if the PQ is full, otherwise false.  List based
//  implementations should always return false.
public boolean isFull() {
    return currentSize == maxSize;
    }
//  Returns an iterator of the objects in the PQ, in no particular
//  order.
public Iterator<E> iterator() {
    return new iteratorHelper();
    }
    class iteratorHelper implements Iterator<E> {
             int index;
long modCheck;
public iteratorHelper() {
        index = 0;
        modCheck = modCounter;
}
public boolean hasNext() {
        if (modCheck != modCounter)
                 throw new ConcurrentModificationException();
        return index < currentSize;
}
public E next() {
}
if(!hasNext())
        throw new NoSuchElementException();
return (E) storage[index++].data;
        public void remove() {
                 throw new UnsupportedOperationException();
                 }
} }